.PHONY: all clean
SHELL := /bin/bash
.ONESHELL:

define JINJA2_CLI
    @mkdir -p $(dir $@)
    @python -m jinja2-cli -t $< -d $(DATA_FILE) -o $@
endef

TEMPLATE_DIR = content
DATA_DIR = data
OUTPUT_DIR = public
DATA_FILE = $(DATA_DIR)/index.json $(DATA_DIR)/blogposts.json $(DATA_DIR)/projects.json $(DATA_DIR)/fonts.json
BLOG_KEY=0b33c5e372d8ee78a8bd842ad0
TEMPLATE_DIRS := $(shell find $(TEMPLATE_DIR) -type d -exec test -e '{}/index.html' ';' -print)
TEMPLATES=$(patsubst $(TEMPLATE_DIR)%,$(OUTPUT_DIR)%/index.html,$(TEMPLATE_DIRS))

all:  $(TEMPLATES) $(DATA_DIR)/blogposts.json $(OUTPUT_DIR)/assets

# Rule for the main target without a subdirectory
$(OUTPUT_DIR)/index.html: $(TEMPLATE_DIR)/index.html $(OUTPUT_DIR)/assets
	$(JINJA2_CLI)

$(OUTPUT_DIR)/%/index.html: $(TEMPLATE_DIR)/%/index.html $(OUTPUT_DIR)/assets
	$(JINJA2_CLI)
	if [ -d "$(TEMPLATE_DIR)/$*/images" ]; then \
		cp -rf $(TEMPLATE_DIR)/$*/images $(OUTPUT_DIR)/$*/; \
	fi

$(OUTPUT_DIR)/assets: assets
	@mkdir -p $(OUTPUT_DIR)
	@cp -rf assets $(OUTPUT_DIR)/
	@npx tailwindcss -i assets/style.css -o $(OUTPUT_DIR)/assets/style.css
	@cp $(TEMPLATE_DIR)/_redirects $(OUTPUT_DIR)
	@cp $(TEMPLATE_DIR)/robots.txt $(OUTPUT_DIR)

$(OUTPUT_DIR)/downloads:
	./update_fonts.sh


$(DATA_DIR)/blogposts.json:
	@mkdir -p $(DATA_DIR)/posts
	@curl -s 'https://blog.smc.org.in/ghost/api/v3/content/posts/?key=$(BLOG_KEY)&include=tags,authors&limit=50' | jq > $@
	slugs=`cat $@ | jq '.posts[].slug' |  tr -d '"' `
	@for slug in $$slugs; do \
		curl -s "https://blog.smc.org.in/ghost/api/v3/content/posts/slug/$${slug}/?key=$(BLOG_KEY)&include=tags,authors" | jq > $(DATA_DIR)/posts/$${slug}.json ;\
		mkdir -p $(OUTPUT_DIR)/blog/$${slug} ;\
		python -m jinja2-cli -t $(TEMPLATE_DIR)/blog/post.html -d $(DATA_FILE) $(DATA_DIR)/posts/$${slug}.json -o $(OUTPUT_DIR)/blog/$${slug}/index.html;\
    done

clean:
	rm -rf public
