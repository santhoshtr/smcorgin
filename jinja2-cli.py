from io import TextIOWrapper
import os
import sys
import json
from argparse import ArgumentParser, FileType
from jinja2 import (
    Environment,
    FileSystemLoader,
)
from jinja_markdown import MarkdownExtension


def render(template_path, data):
    env = Environment(
        loader=FileSystemLoader([".", "content"]),
        keep_trailing_newline=True,
    )
    env.add_extension(MarkdownExtension)
    # Add environ global
    env.globals["environ"] = lambda key: os.environ.get(key)
    env.globals["get_context"] = lambda: data
    return env.get_template(template_path).render(data)


def cli(template_file: TextIOWrapper, data_files: TextIOWrapper, out_file: TextIOWrapper):
    data = {}
    for data_file in data_files:
        json_content = data_file.read()
        data = {**data, **json.loads(json_content)}

    print(f"{template_file.name} > {out_file.name}")
    out_file.write(render(template_file.name, data))
    out_file.flush()
    return 0


def main():
    parser = ArgumentParser("jinja2-cli")
    parser.add_argument(
        "-t",
        "--template",
        type=FileType("r"),
        required=True,
        help="Template File to read, if empty, stdin is used",
    )
    parser.add_argument(
        "-d",
        "--datafiles",
        type=FileType("r"),
        nargs='+',
        required=True,
        help="Data file, in json format",
    )
    parser.add_argument(
        "-o",
        "--outfile",
        type=FileType("w"),
        nargs="?",
        default=sys.stdout,
        help="File to write, if empty, stdout is used",
    )
    args = parser.parse_args()
    sys.exit(cli(args.template, args.datafiles, args.outfile))


if __name__ == "__main__":
    main()
