/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "content/**/*.{html,js}",
  ],

  theme: {
    extend: {},

    fontFamily: {
      sans: ['Manjari', 'sans-serif'],
      serif: ['Malini', 'serif'],
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}

